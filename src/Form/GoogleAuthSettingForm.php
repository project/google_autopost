<?php

namespace Drupal\google_autopost\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures Google Auth settings.
 */
class GoogleAuthSettingForm extends ConfigFormBase {

  /**
   * Constructor for Google Autopost.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {

    parent::__construct($config_factory);
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'google_autopost_config_form';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['config.google_autopost_config'];
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $value = $this->config('config.google_autopost_config');

    $form['google_autopost']['credentials'] = array(
      '#type' => 'fieldset',
      '#title' => t('Credentials'),
      '#description' => t('Fill in the information form as specified in <a href="@link"  target="_blank">Google Developer Console</a>.', array('@link' => 'https://console.developers.google.com/apis/credentials')),
    );

    // Client ID.
    $form['google_autopost']['credentials']['client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Client ID'),
      '#description' => t('OAuth 2.0 client ID'),
      '#default_value' => $value->get('client_id'),
      '#required' => TRUE,
    );

    // Client Secret.
    $form['google_autopost']['credentials']['client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Client Secret'),
      '#description' => t('OAuth 2.0 client secret'),
      '#default_value' => $value->get('client_secret'),
      '#required' => TRUE,
    );

    // Callback URI.
    $form['google_autopost']['credentials']['callback_uri_markup'] = array(
      '#type' => 'item',
      '#title' => t('Callback URI'),
      '#markup' => 'admin/gpost-oauth2callback',
      '#description' => t('OAuth 2.0 callback URI.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    );
    $form['google_autopost']['credentials']['callback_uri'] = array(
      '#type' => 'hidden',
      '#title' => t('Callback URI'),
      '#description' => t('OAuth 2.0 callback URI.'),
      '#default_value' => 'admin/gpost-oauth2callback',
      '#required' => TRUE,
    );

    $form['google_autopost']['gplus'] = array(
      '#type' => 'fieldset',
      '#title' => t('Google+ Account'),
      '#description' => t('Fill in Google+ Account information.'),
    );
    // Account ID.
    $form['google_autopost']['gplus']['google_plus_user_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Google+ User ID'),
      '#description' => t('Submit the <a href="@link" target="_blank">Google+</a> user ID or leave default.', array('@link' => 'https://plus.google.com/')),
      '#default_value' => $value->get('google_plus_user_id') ? $value->get('google_plus_user_id') : 'me',
      '#required' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('config.google_autopost_config')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('callback_uri', $form_state->getValue('callback_uri'))
      ->set('google_plus_user_id', $form_state->getValue('google_plus_user_id'))
      ->set('google_autopost_token', '')
      ->save();

    parent::submitForm($form, $form_state);
  }

}
