<?php

namespace Drupal\google_autopost\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Defines a form that configures Google Auth validate.
 */
class GoogleAuthValidateForm extends ConfigFormBase {

  /**
   * Constructor for GoogleAutopost.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {

    parent::__construct($config_factory);
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'google_autopost_validate_form';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['config.google_autopost_config'];
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('config.google_autopost_config');

    // Get Client ID.
    $client_id = $config->get('client_id');

    // Get Client Secret.
    $client_secret = $config->get('client_secret');

    // Get Callback URI .
    $callback_uri = $config->get('callback_uri');

    // Check if client id OR client secret value has been not been set.
    if (!isset($client_id) || !isset($client_secret) || !isset($callback_uri)) {
      // If not then return with information to set the credentials first before
      // proceding further.
      $form['info'] = array(
        '#type' => 'item',
        '#title' => t('Note'),
        '#markup' => t('<p>You need to fill Google+ Domain API Web Application Credentials settings before proceding with authenticaiton.</p>'),
      );
    }
    else {

      // Get token information form Config.
      $token = $config->get('google_autopost_token');

      // Check if token has been generated.
      // If token is not generated then ask user to authenticate the account.
      if (!isset($token['access_token'])) {

        // Check if library has been loaded.
        $info = \Google_Client::OAUTH2_AUTH_URL;
        if (!$info) {
          // Display informaiton for missing library.
          $form['lib_info'] = array(
            '#type' => 'item',
            '#title' => t('Google Client Library Missing'),
            '#markup' => t("Missing Google Client library. Check Status Reprot or Readme for requiremtents."),
            '#prefix' => '<p>',
            '#suffix' => '</p>',
          );
        }
        else {
          try {
            $client = _google_autopost_client($client_id, $client_secret, $callback_uri);
            $client->setApprovalPrompt('force');
            $auth_url = $client->createAuthUrl();

            // Authentication URL.
            $form['auth_url'] = array(
              '#type' => 'hidden',
              '#default_value' => $auth_url,
            );

            // Submit Button for Authentication.
            $form['auth'] = array(
              '#type' => 'submit',
              '#value' => t('Authorize Google+ Autopost'),
            );
          }
          catch (Exception $e) {
            $form['error'] = array(
              '#type' => 'item',
              '#title' => t('An Error has occured.'),
              '#markup' => t('@data', array('@data' => $e->getMessage())),
              '#prefix' => '<p>',
              '#suffix' => '</p>',
            );
          }
        }
      }
      else {

        // Token data.
        $token_data = $token['access_token'];

        // Token fieldset.
        $form['token'] = array(
          '#type' => 'fieldset',
          '#title' => t('Token Information'),
          '#description' => t('Token information since last authorized.'),
        );

        // Access Token value.
        $form['token']['access_token'] = array(
          '#type' => 'item',
          '#title' => t('Access Token'),
          '#markup' => isset($token_data['access_token']) ? $token_data['access_token'] : 'N/A',
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );

        // Token type.
        $form['token']['token_type'] = array(
          '#type' => 'item',
          '#title' => t('Token Type'),
          '#markup' => isset($token_data['token_type']) ? $token_data['token_type'] : 'N/A',
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );

        // Token expries in seconds.
        $form['token']['expires_in'] = array(
          '#type' => 'item',
          '#title' => t('Expires In'),
          '#markup' => isset($token_data['expires_in']) ? $token_data['expires_in'] : 'N/A',
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );

        // Token ID.
        $form['token']['id_token'] = array(
          '#type' => 'item',
          '#title' => t('Token ID'),
          '#markup' => isset($token_data['id_token']) ? $token_data['id_token'] : 'N/A',
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );

        // Refersh Token.
        $form['token']['refresh_token'] = array(
          '#type' => 'item',
          '#title' => t('Refresh Token'),
          '#markup' => isset($token_data['refresh_token']) ? $token_data['refresh_token'] : 'N/A',
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );

        // Token Last Generated (Server Time).
        $form['token']['created'] = array(
          '#type' => 'item',
          '#title' => t('Created (Server Time)'),
          '#markup' => format_date($token_data['created']),
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );
      }
    }

    return $form;

    // Return parent::buildForm($form, $form_state);.
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if ($values['op']->__toString() == 'Authorize Google+ Autopost') {
      $url = $values['auth_url'];
      $response = new TrustedRedirectResponse($url);
      $form_state->setResponse($response);
    }
  }

}
