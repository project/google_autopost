<?php

namespace Drupal\google_autopost\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a form that configures Google Auth validate.
 */
class Oauth2Callback extends ControllerBase {

  /**
   * {@inheritDoc}.
   *
   * From BlockPluginInterface via BlockBase.
   */
  public function build() {
    $client = new \Google_Client();
    if (!$client) {
      drupal_set_message(t("Can't authenticate with google as library is missing check <a href='@link'>Status Report</a> or README.txt for requirements", array('@link' => url('admin/reports/status'))), 'error');
      return FALSE;
    }

    if (isset($_GET['code'])) {
      try {
        $config = \Drupal::config('config.google_autopost_config');

        $client_id = $config->get('client_id');
        $client_secret = $config->get('client_secret');
        $callback_uri = $config->get('callback_uri');
        $client = _google_autopost_client($client_id, $client_secret, $callback_uri);
        $client->setApprovalPrompt('force');
        $client->authenticate($_GET['code']);
        $access_token = $client->getAccessToken();
        $tokens_decoded = $access_token;
        $refresh_token = $tokens_decoded->refresh_token;
        $data = array(
          'access_token' => $access_token,
          'refresh_token' => $refresh_token,
          'created' => REQUEST_TIME,
        );

        \Drupal::configFactory()->getEditable('config.google_autopost_config')
          ->set('google_autopost_token', $data)
          ->save();
        // variable_set('google_autopost_token', $data);.
        drupal_set_message(t('Google account has been succesfully authenticated.'), 'status');
      }
      catch (Exception $e) {
        drupal_set_message(t('An Error has occured: !data', array('!data' => $e->getMessage())), 'error');
      }
    }
    return new RedirectResponse(\Drupal::url('google_autopost.validate'));
  }

}
